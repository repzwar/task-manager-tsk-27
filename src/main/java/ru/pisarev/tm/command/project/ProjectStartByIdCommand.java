package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().startById(user.getId(), id);
        if (project == null) throw new ProjectNotFoundException();
    }
}
