package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.model.User;

public class TaskClearCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        serviceLocator.getTaskService().clear(user.getId());
    }
}
