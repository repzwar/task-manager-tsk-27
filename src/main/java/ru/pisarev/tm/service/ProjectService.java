package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.service.IProjectService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @Nullable final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public Project findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Project removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Project updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @NotNull
    @Override
    public Project startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @NotNull
    @Override
    public Project startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @NotNull
    @Override
    public Project finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @NotNull
    @Override
    public Project finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @NotNull
    @Override
    public Project finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

}
