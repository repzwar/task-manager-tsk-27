package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    public static final String PASSWORD_SECRET_VALUE = "";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_VALUE = "1";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    public static final String APPLICATION_VERSION_VALUE = "0.25.0";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY))
            return System.getenv(PASSWORD_SECRET_KEY);
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY))
            return System.getProperty(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_VALUE);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) {
            @NotNull final String value = System.getenv(PASSWORD_SECRET_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY)) {
            @NotNull final String value = System.getProperty(PASSWORD_SECRET_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_VALUE);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY))
            return System.getenv(APPLICATION_VERSION_VALUE);
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY))
            return System.getProperty(APPLICATION_VERSION_VALUE);
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_VALUE);
    }

}
